class CreateShortposts < ActiveRecord::Migration
  def change
    create_table :shortposts do |t|
      t.text :body

      t.timestamps
    end
  end
end
