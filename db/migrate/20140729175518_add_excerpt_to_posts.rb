class AddExcerptToPosts < ActiveRecord::Migration
  def self.up
  	change_table(:posts) do |t|
  		t.string :excerpt
  	end
  end

  def self.down
  	remove_column :posts, :excerpt
  	remove_column :posts_translations, :excerpt
  end
end