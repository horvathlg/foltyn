class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :body
      t.date :created_at
      t.date :updated_at

      t.timestamps
    end
  end
end
