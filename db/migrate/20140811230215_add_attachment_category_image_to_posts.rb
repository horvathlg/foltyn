class AddAttachmentCategoryImageToPosts < ActiveRecord::Migration
  def self.up
    change_table :posts do |t|
      t.attachment :category_image
    end
  end

  def self.down
    remove_attachment :posts, :category_image
  end
end
