class CreateShortpostTranslations < ActiveRecord::Migration
  def self.up
    Shortpost.create_translation_table!({
    	body: :string
    }, {
    	migrate_data: true
    })
  end

  def self.down
  	Shortpost.drop_translation_table! migrate_data: true
  end
end
