class PostsController < ApplicationController
    before_action :set_post, only: [:show]

    # GET /posts
    # GET /posts.json
    def tag_cloud
      @tags = Post.tag_counts_on(:tags)
    end

    def index
      if params[:tag]
        @posts = Post.tagged_with(params[:tag])
                  .page(params[:page])
                  .paginate(page: params[:page], per_page: 5)
                  .order('created_at desc')
        
        @shortposts = Shortpost.all.shuffle[0..4]
        @posts_archive = Post.all.group_by { |t| t.created_at.beginning_of_month }
      elsif params[:query]
        @posts = Post.search(params[:query])
        
        @shortposts = Shortpost.all.shuffle[0..4]
        @posts_archive = Post.all.group_by { |t| t.created_at.beginning_of_month }
      else
        @posts = Post.all.order('created_at desc')
                  .paginate(page: params[:page], per_page: 5)
                  .order('created_at desc')
        
        @shortposts = Shortpost.all.shuffle[0..4]
        @posts_archive =  Post.all.order('created_at desc')
                                  .group_by { |t| t.created_at.beginning_of_month }
      end

    end

    # GET /posts/1
    # GET /posts/1.json
    def show
      @related_posts = Post.joins(:taggings)
                  .where('posts.id != ?', @post.id)
                  .where(taggings: { tag_id: @post.tag_ids })
                  .all.shuffle[0..3]
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_post
        @post = Post.find(params[:id])
      end
end
