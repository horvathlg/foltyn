class ShortpostsController < ApplicationController
  before_action :set_shortpost, only: [:show]

  # GET /shortposts
  # GET /shortposts.json
  def index
    @shortposts = Shortpost.all
  end

  # GET /shortposts/1
  # GET /shortposts/1.json
  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shortpost
      @shortpost = Shortpost.find(params[:id])
    end
end
