module Admin
  class ShortpostsController < ApplicationController
    before_action :set_shortpost, only: [:show, :edit, :update, :destroy]

    # GET /shortposts
    # GET /shortposts.json
    def index
      @shortposts = Shortpost.all
    end

    # GET /shortposts/1
    # GET /shortposts/1.json
    def show
    end

    # GET /shortposts/new
    def new
      @shortpost = Shortpost.new
    end

    # GET /shortposts/1/edit
    def edit
    end

    # POST /shortposts
    # POST /shortposts.json
    def create
      @shortpost = Shortpost.new(shortpost_params)

      respond_to do |format|
        if @shortpost.save
          format.html { redirect_to @shortpost, notice: 'Shortpost was successfully created.' }
          format.json { render :show, status: :created, location: @shortpost }
        else
          format.html { render :new }
          format.json { render json: @shortpost.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /shortposts/1
    # PATCH/PUT /shortposts/1.json
    def update
      respond_to do |format|
        if @shortpost.update(shortpost_params)
          format.html { redirect_to @shortpost, notice: 'Shortpost was successfully updated.' }
          format.json { render :show, status: :ok, location: @shortpost }
        else
          format.html { render :edit }
          format.json { render json: @shortpost.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /shortposts/1
    # DELETE /shortposts/1.json
    def destroy
      @shortpost.destroy
      respond_to do |format|
        format.html { redirect_to shortposts_url, notice: 'Shortpost was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_shortpost
        @shortpost = Shortpost.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def shortpost_params
        params.require(:shortpost).permit(:body)
      end
  end
end