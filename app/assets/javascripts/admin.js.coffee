validateLength = (form, maxLength) ->
	if $(form).length
		$(document).on 'keyup', ->

			count = $(form).val().length

			$("#validation-length").html("<p>Excerpts length: #{count}<p>")

			if count > maxLength
				$(form).css('border', '3px solid red')
				$("#validation-length").append("<p>Excerpt length can\'t be over 400!</p>")
															 .css("color", "red")
			else 
				$(form).css('border', '1px solid #cccccc')

validatePresence = (form) ->
	if $(form).length
		$(document).on 'click', ->

			if $(form).val().length == 0
				$(form).css('border', '3px solid red')
				$("#validation-presence").append("<p>Can't be blank!</p>")
																 .css("color", "red")
			else 
				$(form).css('border', '1px solid #cccccc')

$ ->
	$('.datatable').dataTable({
		})

	validateLength('#post_excerpt', 400)
	validatePresence('#post_title')
	validatePresence('#post_excerpt')
	validatePresence('#post_tag_list')