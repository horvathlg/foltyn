json.array!(@shortposts) do |shortpost|
  json.extract! shortpost, :id, :body
  json.url shortpost_url(shortpost, format: :json)
end
