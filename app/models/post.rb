class Post < ActiveRecord::Base
	
	validates :title, :excerpt, :body, presence: true
	validates :title, uniqueness: true
	validates :excerpt, length: { maximum: 400 }

	has_attached_file :category_image, styles: { medium: "300x300>", thumb: "30x30>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :category_image, content_type: /\Aimage\/.*\Z/
	#titles as urls
	extend FriendlyId
	friendly_id :title, use: [:slugged, :finders]
	#internationalization
	translates :title, :body, :excerpt
	#tagging
	acts_as_taggable
	#search
	searchkick
	#forum
	DISQUS_SHORTNAME = Rails.env == "development" ? "dev_shortname".freeze : "production_shortname".freeze
end
