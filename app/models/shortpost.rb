class Shortpost < ActiveRecord::Base
	validates :body, presence: true
	validates :body, length: { maximum: 200 }

	translates :body
end
